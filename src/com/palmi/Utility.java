package com.palmi;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Utility {
    private Utility() {
    }

    public static Map<String, Long> getFilesFoldersCount(Stream<Path> pathStream) throws IOException {
        return getFileStream(pathStream)
            .map(Utility::getExtension)
            .collect(Collectors.groupingBy(fileExt -> fileExt, Collectors.counting()))
        ;
    }

    public static Stream<File> getFileStream(Stream<Path> streamPath) throws IOException {
        return streamPath.map(Path::toFile)
                .flatMap(file -> {
                    try {
                        return file.isDirectory() ?
                                Stream.concat(Stream.of(file), getFileStream(Files.list(Paths.get(file.getAbsolutePath())))) :
                                Stream.of(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                });
    }

    public static String getExtension(File file){
        return file.isDirectory() ?
                "folders" :
                file.getName().substring(file.getName().lastIndexOf(".") + 1);
    }

    public static String userPath() {
        System.out.println("Inserisci un percorso valido ed esistente (C:\\cartella-1\\cartella-2\\...)");
        return new Scanner(System.in).next();
    }

    public static Path getPath (String path) {
        try {
            return  Paths.get(path).toFile().canRead() && Paths.get(path).toFile().isDirectory() ?
                    Paths.get(path) :
                    getPath(userPath());
        } catch (InvalidPathException e) {
            getPath(userPath());
        }
        return null;
    }

}

